#!/bin/bash

# Get accurate values for the script name, absolute path of the script and the combination of the two.
pushd `dirname "$0"` > /dev/null
SCRIPT_NAME=`basename "$0"`
SCRIPT_PATH=`pwd`
SCRIPT_FULLPATH="${SCRIPT_PATH}/${SCRIPT_NAME}"
popd > /dev/null

# Check if openssl is in the path, otherwise, exit the script.
command -v openssl >/dev/null 2>&1 || { echo >&2 "openssl could not be found.  Aborting."; exit 1; }

# The following script is heavely based on this page, with some minor tweaks:
# https://gist.github.com/jchandra74/36d5f8d0e11960dd8f80260801109ab0

CERT_PATH="${SCRIPT_PATH}/../.cert"
# Fetch abosulte full path.
CERT_PATH="$(cd "$(dirname "$CERT_PATH")"; pwd)/$(basename "$CERT_PATH")"

# Make sure the cert folder is present.
mkdir -p "${CERT_PATH}"

# Create the certificate database.
echo '01' > "${CERT_PATH}/serial"
touch "${CERT_PATH}/index.txt"

# Create the certificate authority (CA) configuration file.
mkdir -p "${CERT_PATH}/certs"
mkdir -p "${CERT_PATH}/crl"
mkdir -p "${CERT_PATH}/newcerts"
mkdir -p "${CERT_PATH}/private"
rm -f "${CERT_PATH}/caconfig.cnf"
cat > "${CERT_PATH}/caconfig.cnf" <<EOF
# Default configuration to use when one is not provided on the command line.
[ ca ]
default_ca = local_ca

# Default location of directories and files needed to generate certificates.
[ local_ca ]
dir           = ${CERT_PATH}   # Where everything is kept
certs         = \$dir/certs      # Where the issued certs are kept
crl_dir       = \$dir/crl        # Where the issued crl are kept
database      = \$dir/index.txt  # database index file.
new_certs_dir = \$dir/newcerts   # default place for new certs.
certificate   = \$dir/cacert.pem # The CA certificate
serial        = \$dir/serial     # The current serial number
crlnumber     = \$dir/crlnumber  # the current crl number
                          # must be commented out to leave a V1 CRL
crl           = \$dir/crl.pem    # The current CRL
private_key   = \$dir/private/cakey.pem # The private key
RANDFILE      = \$dir/private/.rand # private random number file

# Default expiration and encryption policies for certificates.
default_crl_days = 365  # how long before next CRL
default_days = 1825     # how long to certify for
preserve	= no        # keep passed DN ordering

# sha1 is no longer recommended, we will be using sha256.
default_md = sha256

policy = local_ca_policy
x509_extensions = local_ca_extensions

# Copy extensions specified in the certificate request.
copy_extensions = copy

# Comment out the following two lines for the "traditional"
# (and highly broken) format.
name_opt = ca_default  # Subject Name options
cert_opt = ca_default  # Certificate field options

# Default policy to use when generating server certificates. 
# The following fields must be defined in the server certificate.
#
# DO NOT CHANGE "supplied" BELOW TO ANYTHING ELSE.
# It is the correct content.
[ local_ca_policy ]
commonName = supplied
stateOrProvinceName = supplied
countryName = supplied
emailAddress = supplied
organizationName = supplied
organizationalUnitName = supplied

# x509 extensions to use when generating server certificates.
[ local_ca_extensions ]
basicConstraints = CA:FALSE

# The default root certificate generation policy.
[ req ]
default_bits = 2048
default_keyfile = ${CERT_PATH}/private/cakey.pem

# sha1 is no longer recommended, we will be using sha256.
default_md = sha256

prompt = no
distinguished_name = root_ca_distinguished_name
x509_extensions = root_ca_extensions

# Root Certificate Authority distinguished name.
#
# DO CHANGE THE CONTENT OF THESE FIELDS TO MATCH YOUR OWN SETTINGS!
[ root_ca_distinguished_name ]
commonName = OwnNotif Root Certificate Authority
stateOrProvinceName = NY
countryName = US
emailAddress = does.not.exist@ownnotif.com
organizationName = OwnNotif
organizationalUnitName = Development

[ root_ca_extensions ]
basicConstraints = CA:TRUE
EOF

ORIGINAL_OPENSSL_CONF="$OPENSSL_CONF"
export OPENSSL_CONF="${CERT_PATH}/caconfig.cnf"

# Creating the test certificate authority (CA).
# Generate the certificate authority (CA) certificate without pass phrase (-nodes).
openssl req -nodes -x509 -newkey rsa:4096 -out "${CERT_PATH}/cacert.pem" -outform PEM -days 1825

# If you are on Windows, you will be using .crt file instead.
openssl x509 -in "${CERT_PATH}/cacert.pem" -out "${CERT_PATH}/cacert.crt"

# Creating a self-signed certificate with subject alternative name (SAN).
rm -f "${CERT_PATH}/localhost.cnf"
cat > "${CERT_PATH}/localhost.cnf" <<EOF
[ req ]
prompt = no
distinguished_name = server_distinguished_name
req_extensions = v3_req

[ server_distinguished_name ]
commonName = 127.0.0.1
stateOrProvinceName = NY
countryName = US
emailAddress = does.not.exist@ownnotif.com
organizationName = OwnNotif
organizationalUnitName = Development

[ v3_req ]
basicConstraints = CA:FALSE
keyUsage = nonRepudiation, digitalSignature, keyEncipherment
subjectAltName = @alt_names

[ alt_names ]
DNS.0 = localhost
DNS.1 = ownnotif-server-api.local
IP.0 = 127.0.0.1
EOF

export OPENSSL_CONF="${CERT_PATH}/localhost.cnf"

# Generate the certificate and key.
openssl req -nodes -newkey rsa:2048 -keyout "${CERT_PATH}/tempkey.pem" -keyform PEM -out "${CERT_PATH}/tempreq.pem" -outform PEM

# Create the unencrypted key file
openssl rsa < "${CERT_PATH}/tempkey.pem" > "${CERT_PATH}/server_key.pem"

# Switch back the export to caconfig.cnf so we can sign the new certificate request with the CA.
export OPENSSL_CONF="${CERT_PATH}/caconfig.cnf"

# Sign it.
openssl ca -batch -in "${CERT_PATH}/tempreq.pem" -out "${CERT_PATH}/server_crt.pem"

# In Windows, we mostly use .pfx and .crt files. Therefore, we need to convert the .pem file to .pfx. 
# We'll use cat to combine server_key.pem and server_crt.pem into a file called hold.pem. Then we will 
# do the conversion using openssl pkcs12 command as shown below. You can use whatever text you want to 
# describe your new .pfx file in the -name parameter.
cat "${CERT_PATH}/server_key.pem" "${CERT_PATH}/server_crt.pem" > "${CERT_PATH}/hold.pem"
openssl pkcs12 -export -nodes -out "${CERT_PATH}/localhost.pfx" -in "${CERT_PATH}/hold.pem" -passout pass: -name "OwnNotif Dev Self-Signed SSL Certificate"

# Creates dhparam.pem file used for DHE ciphersuites.
openssl dhparam -out "${CERT_PATH}/dhparam.pem" 2048

export OPENSSL_CONF="${ORIGINAL_OPENSSL_CONF}"

exit 0
