@ECHO OFF

:: Check if openssl is in the path, otherwise, exit the script.
openssl version >NUL 2>&1
IF "%ERRORLEVEL%" == "9009" (
    ECHO openssl could not be found. Aborting.
    EXIT /B 1
)

:: Fetch absolute full path of the script, without script name/ext.
PUSHD %~dp0
SET SCRIPT_PATH=%CD%
POPD
:: Remove trailing backslash.
SET SCRIPT_PATH=%SCRIPT_PATH:~0,-1%

:: The following script is heavely based on this page, with some minor tweaks:
:: https://gist.github.com/jchandra74/36d5f8d0e11960dd8f80260801109ab0

:: Fetch parent folder.
FOR %%A IN ("%SCRIPT_PATH%") DO SET "CERT_PATH=%%~dpA"
:: Remove trailing backslash.
SET CERT_PATH=%CERT_PATH:~0,-1%
:: Point to cert folder.
SET CERT_PATH=%CERT_PATH%\.cert
:: Cert folder with forward slash.
SET CERT_PATH_FORWARD=%CERT_PATH:\=/%

:: Make sure the cert folder is present.
IF NOT EXIST "%CERT_PATH%" MKDIR "%CERT_PATH%"

:: Create the certificate database.
ECHO 01> "%CERT_PATH%\serial"
TYPE NUL > "%CERT_PATH%\index.txt"

:: Create the certificate authority (CA) configuration file.
IF NOT EXIST "%CERT_PATH%\certs" MKDIR "%CERT_PATH%\certs"
IF NOT EXIST "%CERT_PATH%\crl" MKDIR "%CERT_PATH%\crl"
IF NOT EXIST "%CERT_PATH%\newcerts" MKDIR "%CERT_PATH%\newcerts"
IF NOT EXIST "%CERT_PATH%\private" MKDIR "%CERT_PATH%\private"
IF EXIST "%CERT_PATH%\caconfig.cnf" DEL /F /Q "%CERT_PATH%\caconfig.cnf"

ECHO # Default configuration to use when one is not provided on the command line.>> "%CERT_PATH%\caconfig.cnf"
ECHO [ ca ]>> "%CERT_PATH%\caconfig.cnf"
ECHO default_ca = local_ca>> "%CERT_PATH%\caconfig.cnf"
ECHO.>> "%CERT_PATH%\caconfig.cnf"
ECHO # Default location of directories and files needed to generate certificates.>> "%CERT_PATH%\caconfig.cnf"
ECHO [ local_ca ]>> "%CERT_PATH%\caconfig.cnf"
ECHO dir           = %CERT_PATH_FORWARD%  # Where everything is kept>> "%CERT_PATH%\caconfig.cnf"
ECHO certs         = $dir/certs      # Where the issued certs are kept>> "%CERT_PATH%\caconfig.cnf"
ECHO crl_dir       = $dir/crl        # Where the issued crl are kept>> "%CERT_PATH%\caconfig.cnf"
ECHO database      = $dir/index.txt  # database index file.>> "%CERT_PATH%\caconfig.cnf"
ECHO new_certs_dir = $dir/newcerts   # default place for new certs.>> "%CERT_PATH%\caconfig.cnf"
ECHO certificate   = $dir/cacert.pem # The CA certificate>> "%CERT_PATH%\caconfig.cnf"
ECHO serial        = $dir/serial     # The current serial number>> "%CERT_PATH%\caconfig.cnf"
ECHO crlnumber     = $dir/crlnumber  # the current crl number>> "%CERT_PATH%\caconfig.cnf"
ECHO                         # must be commented out to leave a V1 CRL>> "%CERT_PATH%\caconfig.cnf"
ECHO crl           = $dir/crl.pem    # The current CRL>> "%CERT_PATH%\caconfig.cnf"
ECHO private_key   = $dir/private/cakey.pem # The private key>> "%CERT_PATH%\caconfig.cnf"
ECHO RANDFILE      = $dir/private/.rand # private random number file>> "%CERT_PATH%\caconfig.cnf"
ECHO.>> "%CERT_PATH%\caconfig.cnf"
ECHO # Default expiration and encryption policies for certificates.>> "%CERT_PATH%\caconfig.cnf"
ECHO default_crl_days = 365  # how long before next CRL>> "%CERT_PATH%\caconfig.cnf"
ECHO default_days = 1825     # how long to certify for>> "%CERT_PATH%\caconfig.cnf"
ECHO preserve	= no        # keep passed DN ordering>> "%CERT_PATH%\caconfig.cnf"
ECHO.>> "%CERT_PATH%\caconfig.cnf"
ECHO # sha1 is no longer recommended, we will be using sha256.>> "%CERT_PATH%\caconfig.cnf"
ECHO default_md = sha256>> "%CERT_PATH%\caconfig.cnf"
ECHO.>> "%CERT_PATH%\caconfig.cnf"
ECHO policy = local_ca_policy>> "%CERT_PATH%\caconfig.cnf"
ECHO x509_extensions = local_ca_extensions>> "%CERT_PATH%\caconfig.cnf"
ECHO.>> "%CERT_PATH%\caconfig.cnf"
ECHO # Copy extensions specified in the certificate request.>> "%CERT_PATH%\caconfig.cnf"
ECHO copy_extensions = copy>> "%CERT_PATH%\caconfig.cnf"
ECHO.>> "%CERT_PATH%\caconfig.cnf"
ECHO # Comment out the following two lines for the "traditional">> "%CERT_PATH%\caconfig.cnf"
ECHO # (and highly broken) format.>> "%CERT_PATH%\caconfig.cnf"
ECHO name_opt = ca_default  # Subject Name options>> "%CERT_PATH%\caconfig.cnf"
ECHO cert_opt = ca_default  # Certificate field options>> "%CERT_PATH%\caconfig.cnf"
ECHO.>> "%CERT_PATH%\caconfig.cnf"
ECHO # Default policy to use when generating server certificates.>> "%CERT_PATH%\caconfig.cnf"
ECHO # The following fields must be defined in the server certificate.>> "%CERT_PATH%\caconfig.cnf"
ECHO #>> "%CERT_PATH%\caconfig.cnf"
ECHO # DO NOT CHANGE "supplied" BELOW TO ANYTHING ELSE.>> "%CERT_PATH%\caconfig.cnf"
ECHO # It is the correct content.>> "%CERT_PATH%\caconfig.cnf"
ECHO [ local_ca_policy ]>> "%CERT_PATH%\caconfig.cnf"
ECHO commonName = supplied>> "%CERT_PATH%\caconfig.cnf"
ECHO stateOrProvinceName = supplied>> "%CERT_PATH%\caconfig.cnf"
ECHO countryName = supplied>> "%CERT_PATH%\caconfig.cnf"
ECHO emailAddress = supplied>> "%CERT_PATH%\caconfig.cnf"
ECHO organizationName = supplied>> "%CERT_PATH%\caconfig.cnf"
ECHO organizationalUnitName = supplied>> "%CERT_PATH%\caconfig.cnf"
ECHO.>> "%CERT_PATH%\caconfig.cnf"
ECHO # x509 extensions to use when generating server certificates.>> "%CERT_PATH%\caconfig.cnf"
ECHO [ local_ca_extensions ]>> "%CERT_PATH%\caconfig.cnf"
ECHO basicConstraints = CA:FALSE>> "%CERT_PATH%\caconfig.cnf"
ECHO.>> "%CERT_PATH%\caconfig.cnf"
ECHO # The default root certificate generation policy.>> "%CERT_PATH%\caconfig.cnf"
ECHO [ req ]>> "%CERT_PATH%\caconfig.cnf"
ECHO default_bits = 2048>> "%CERT_PATH%\caconfig.cnf"
ECHO default_keyfile = %CERT_PATH_FORWARD%/private/cakey.pem>> "%CERT_PATH%\caconfig.cnf"
ECHO.>> "%CERT_PATH%\caconfig.cnf"
ECHO # sha1 is no longer recommended, we will be using sha256.>> "%CERT_PATH%\caconfig.cnf"
ECHO default_md = sha256>> "%CERT_PATH%\caconfig.cnf"
ECHO.>> "%CERT_PATH%\caconfig.cnf"
ECHO prompt = no>> "%CERT_PATH%\caconfig.cnf"
ECHO distinguished_name = root_ca_distinguished_name>> "%CERT_PATH%\caconfig.cnf"
ECHO x509_extensions = root_ca_extensions>> "%CERT_PATH%\caconfig.cnf"
ECHO.>> "%CERT_PATH%\caconfig.cnf"
ECHO # Root Certificate Authority distinguished name.>> "%CERT_PATH%\caconfig.cnf"
ECHO #>> "%CERT_PATH%\caconfig.cnf"
ECHO # DO CHANGE THE CONTENT OF THESE FIELDS TO MATCH YOUR OWN SETTINGS!>> "%CERT_PATH%\caconfig.cnf"
ECHO [ root_ca_distinguished_name ]>> "%CERT_PATH%\caconfig.cnf"
ECHO commonName = OwnNotif Root Certificate Authority>> "%CERT_PATH%\caconfig.cnf"
ECHO stateOrProvinceName = NY>> "%CERT_PATH%\caconfig.cnf"
ECHO countryName = US>> "%CERT_PATH%\caconfig.cnf"
ECHO emailAddress = does.not.exist@ownnotif.com>> "%CERT_PATH%\caconfig.cnf"
ECHO organizationName = OwnNotif>> "%CERT_PATH%\caconfig.cnf"
ECHO organizationalUnitName = Development>> "%CERT_PATH%\caconfig.cnf"
ECHO.>> "%CERT_PATH%\caconfig.cnf"
ECHO [ root_ca_extensions ]>> "%CERT_PATH%\caconfig.cnf"
ECHO basicConstraints = CA:TRUE>> "%CERT_PATH%\caconfig.cnf"

SET ORIGINAL_OPENSSL_CONF=%OPENSSL_CONF%
SET OPENSSL_CONF=%CERT_PATH%\caconfig.cnf

:: Creating the test certificate authority (CA).
:: Generate the certificate authority (CA) certificate without pass phrase (-nodes).
openssl req -nodes -x509 -newkey rsa:4096 -out "%CERT_PATH%\cacert.pem" -outform PEM -days 1825

:: If you are on Windows, you will be using .crt file instead.
openssl x509 -in "%CERT_PATH%\cacert.pem" -out "%CERT_PATH%\cacert.crt"

:: Creating a self-signed certificate with subject alternative name (SAN).
IF EXIST "%CERT_PATH%\localhost.cnf" DEL /F /Q "%CERT_PATH%\localhost.cnf"
ECHO [ req ]>> "%CERT_PATH%\localhost.cnf"
ECHO prompt = no>> "%CERT_PATH%\localhost.cnf"
ECHO distinguished_name = server_distinguished_name>> "%CERT_PATH%\localhost.cnf"
ECHO req_extensions = v3_req>> "%CERT_PATH%\localhost.cnf"
ECHO.>> "%CERT_PATH%\localhost.cnf"
ECHO [ server_distinguished_name ]>> "%CERT_PATH%\localhost.cnf"
ECHO commonName = 127.0.0.1>> "%CERT_PATH%\localhost.cnf"
ECHO stateOrProvinceName = NY>> "%CERT_PATH%\localhost.cnf"
ECHO countryName = US>> "%CERT_PATH%\localhost.cnf"
ECHO emailAddress = does.not.exist@ownnotif.com>> "%CERT_PATH%\localhost.cnf"
ECHO organizationName = OwnNotif>> "%CERT_PATH%\localhost.cnf"
ECHO organizationalUnitName = Development>> "%CERT_PATH%\localhost.cnf"
ECHO.>> "%CERT_PATH%\localhost.cnf"
ECHO [ v3_req ]>> "%CERT_PATH%\localhost.cnf"
ECHO basicConstraints = CA:FALSE>> "%CERT_PATH%\localhost.cnf"
ECHO keyUsage = nonRepudiation, digitalSignature, keyEncipherment>> "%CERT_PATH%\localhost.cnf"
ECHO subjectAltName = @alt_names>> "%CERT_PATH%\localhost.cnf"
ECHO.>> "%CERT_PATH%\localhost.cnf"
ECHO [ alt_names ]>> "%CERT_PATH%\localhost.cnf"
ECHO DNS.0 = localhost>> "%CERT_PATH%\localhost.cnf"
ECHO DNS.1 = ownnotif-server-api.local>> "%CERT_PATH%\localhost.cnf"
ECHO IP.0 = 127.0.0.1>> "%CERT_PATH%\localhost.cnf"

SET OPENSSL_CONF=%CERT_PATH%\localhost.cnf

:: Generate the certificate and key.
openssl req -nodes -newkey rsa:2048 -keyout "%CERT_PATH%\tempkey.pem" -keyform PEM -out "%CERT_PATH%\tempreq.pem" -outform PEM

:: Create the unencrypted key file
openssl rsa < "%CERT_PATH%\tempkey.pem" > "%CERT_PATH%\server_key.pem"

:: Switch back the export to caconfig.cnf so we can sign the new certificate request with the CA.
SET OPENSSL_CONF=%CERT_PATH%\caconfig.cnf

:: Sign it.
openssl ca -batch -in "%CERT_PATH%\tempreq.pem" -out "%CERT_PATH%\server_crt.pem"

:: In Windows, we mostly use .pfx and .crt files. Therefore, we need to convert the .pem file to .pfx. 
:: We'll use cat to combine server_key.pem and server_crt.pem into a file called hold.pem. Then we will 
:: do the conversion using openssl pkcs12 command as shown below. You can use whatever text you want to 
:: describe your new .pfx file in the -name parameter.
TYPE "%CERT_PATH%\server_key.pem" "%CERT_PATH%\server_crt.pem" > "%CERT_PATH%\hold.pem"
openssl pkcs12 -export -nodes -out "%CERT_PATH%\localhost.pfx" -in "%CERT_PATH%\hold.pem" -passout pass: -name "OwnNotif Dev Self-Signed SSL Certificate"

:: Creates dhparam.pem file used for DHE ciphersuites.
openssl dhparam -out "%CERT_PATH%\dhparam.pem" 2048

SET OPENSSL_CONF="%ORIGINAL_OPENSSL_CONF%"

EXIT /B 0
