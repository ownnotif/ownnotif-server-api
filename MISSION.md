# Mission Statement

OwnNotif strive to be a secure portable solution to send push notifications to your devices. Nobody else does it free, openly and, most importantly, securely.

More [free (libre)](https://www.gnu.org/philosophy/free-sw.en.html) and open source softwares is key to a culture's growth and this kind of software, a push notification system, is sorely lacking in the world. Some solution exists, though either you have to trust closed softwares with your data (and sometime pay for the privilege) or look into the very few free softwares available that offers no security for your data and/or are mostly abandoned.

We propose OwnNotif as a cure all solution for all those predicaments. Security is important to us, as well as the privacy of our data. The less closed third parties involved in manipulating our data, the better off we will be in the world. The cloud can be great for some applications, but don't forget that the cloud is simply someone else computer.

Community participation is key to OwnNotif's success, and contributions are desired from as many people as possible. Therefore, we adopt the [Collective Code Construction Contract (C4)](https://rfc.ownotif.com/spec-2/C4), a proven collaboration model developed by other successful projects such as [ZeroMQ](http://zeromq.org/).

# Project Direction

The OwnNotif project direction is the sum of documented problems: everybody is invited to describe and discuss a problem in the GitLab issue tracker. Contributed solutions to those problems will be merged according to [C4](https://rfc.ownnotif.com/spec-2/C4) rules, which are designed to encourage participation.

Some problem fields we initially focus on are:

* Have a simple server API to serve push notifications.
* A web interface for the server management (users, settings, etc.).
* At least one client to test those push notifications (web or Android, not decided yet).
* Following [OWASP cheat sheets](https://www.owasp.org/index.php/OWASP_Cheat_Sheet_Series) to have a very secure system.
