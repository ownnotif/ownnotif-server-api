"use strict";

const path = require("path");
const https = require("https");

const localApp = require(path.resolve("./src/app.js"));
const app = localApp.app;
const httpsOptions = localApp.httpsOptions;

// For testing.
module.exports = app;

// Server creation.
const server = https.createServer(httpsOptions, app);

// Setting up a grateful exit, since the server will run forever.
function exitGracefully() {
    server.close(function() {
        console.log("Closing event received. Server will shutdown.");
    });
}
// Caught event that works on Windows and *nix system. (CTRL+C and such).
process.on("SIGTERM", exitGracefully);
process.on("SIGINT", exitGracefully);

// Launch the server.
server.listen(httpsOptions.port, httpsOptions.host, () => {
    console.log("%s:%s listening at %s", app.get("name"), app.get("version"), app.get("url"));
});
