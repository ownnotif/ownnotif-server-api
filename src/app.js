"use strict";

const path = require("path");
const bodyParser = require("body-parser");
const express = require("express");
const app = express();

const defaults = require(path.resolve("./src/util/defaults.js"));
const serverConfig = require(path.resolve("./src/util/validation/server-config.js"));
const routes = require(path.resolve("./src/routes/routes.js"));

app.set("name", defaults.server.name);
app.set("version", defaults.server.version);
app.set("url", "https://" + serverConfig.httpsOptions.host + ":" + serverConfig.httpsOptions.port);

// Setup server middleware.
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Routes.
routes(app);

// Export is required for unit/integration testing.
module.exports.app = app;
module.exports.httpsOptions = serverConfig.httpsOptions;
