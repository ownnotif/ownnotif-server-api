"use strict";

const path = require("path");
const httpMocks = require("node-mocks-http");

const helloRoutes = require(path.resolve("./src/routes/hello.js"));

describe("GET /hello/:name", () => {
    it("should echo the world", () => {
        let req = httpMocks.createRequest({
            method: "GET",
            url: "/hello/world",
            params: {
                name: "world"
            }
        });
        let res = httpMocks.createResponse();

        helloRoutes.getName(req, res);

        let data = JSON.parse(res._getData());
        expect(data.name).toEqual("world");
        expect(res.statusCode).toEqual(200);
        expect(res._isEndCalled()).toBeTruthy();
        expect(res._isJSON()).toBeTruthy();
        expect(res._isUTF8()).toBeTruthy();
    });
});
