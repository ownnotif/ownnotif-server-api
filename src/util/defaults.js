"use strict";

const path = require("path");
const pkg = require(path.resolve("./package.json"));

module.exports = {
    server: {
        name: pkg["name"],
        version: pkg["version"]
    },

    host: "localhost",
    port: 8433,

    security: {
        privateKeyPath: "./.cert/server_key.pem",
        certChainsPath: "./.cert/server_crt.pem",
        caCertsPath: "./.cert/cacert.pem",
        dhparamPath: "./.cert/dhparam.pem",

        // [TODO] Once Let's Encrypt support ECDSA, forget RSA ciphers and use the next line instead.
        // ETA: July, 2018
        // ciphers: 'ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-ECDSA-AES256-SHA384:ECDHE-ECDSA-AES128-SHA:ECDHE-ECDSA-AES256-SHA'
        ciphers:
            "ECDHE-RSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES128-SHA256:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES128-SHA:ECDHE-RSA-AES256-SHA:ECDHE-RSA-RC4-SHA",
        honorCipherOrder: true,

        // [TODO] Can't seem to activate a specific ECDH curve in Node.js. For now, leave it
        // at 'auto', but should look into it further down the road.
        //
        // 384 bit prime modulus curve efficiently supports ECDHE ssl_ciphers up to a SHA384 hash.
        //ecdhCurve: 'secp384r1',
        ecdhCurve: "auto",

        secureProtocol: "TLSv1_2_method"
    }
};
