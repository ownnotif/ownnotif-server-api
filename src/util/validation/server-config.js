"use strict";

const path = require("path");
const https = require("https");
const fs = require("fs");
const defaults = require(path.resolve("./src/util/defaults.js"));
const caValidation = require(path.resolve("./src/util/validation/ca.js"));

function onFatalError(msg) {
    console.error(msg);
    process.exit(1);
}

let privateKey;
let certChains;
let caCerts;
let dhparam;

try {
    privateKey = path.resolve(
        process.env.npm_package_config_tls_private_key_path || defaults.security.privateKeyPath
    );
} catch (err) {
    onFatalError("The config tls-private-key-path is not a valid path.");
}

try {
    certChains = path.resolve(
        process.env.npm_package_config_tls_cert_chains_path || defaults.security.certChainsPath
    );
} catch (err) {
    onFatalError("The config tls-cert-chains-path is not a valid path.");
}

try {
    caCerts = path.resolve(
        process.env.npm_package_config_tls_ca_certs_path || defaults.security.caCertsPath
    );
} catch (err) {
    onFatalError("The config tls-ca-certs-path is not a valid path.");
}

try {
    dhparam = path.resolve(
        process.env.npm_package_config_dhparam_path || defaults.security.dhparamPath
    );
} catch (err) {
    onFatalError("The config dhparam-path is not a valid path.");
}

try {
    caValidation.ensureCAExistence(privateKey, certChains, caCerts, dhparam);
} catch (err) {
    onFatalError(err.message);
}

// Setup HTTPS global options.
https.globalAgent.options = Object.assign({}, https.globalAgent.options, {
    rejectUnauthorized: true,

    key: fs.readFileSync(privateKey),
    cert: fs.readFileSync(certChains),
    ca: fs.readFileSync(caCerts),

    ciphers: defaults.security.ciphers,
    honorCipherOrder: defaults.security.honorCipherOrder,
    ecdhCurve: defaults.security.ecdhCurve,

    dhparam: fs.readFileSync(dhparam),
    secureProtocol: defaults.security.secureProtocol
});

// Setup HTTPS local options.
module.exports.httpsOptions = Object.assign({}, https.globalAgent.options, {
    host: process.env.HOST || process.env.npm_package_config_host || defaults.host,
    port: process.env.PORT || process.env.npm_package_config_port || defaults.port,
    sessionTimeout: 60
});
