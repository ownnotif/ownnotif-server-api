"use strict";

const path = require("path");
const fs = require("fs");

const createCertsPathWindows = path.resolve("./scripts/create_test_cert.bat");
const createCertsPathPosix = path.resolve("./scripts/create_test_cert.sh");

module.exports.ensureCAExistence = (tlsKey, tlsCert, tlsCA, tlsDHParam) => {
    try {
        fs.accessSync(tlsKey, fs.constants.F_OK | fs.constants.R_OK);
        fs.accessSync(tlsCert, fs.constants.F_OK | fs.constants.R_OK);
        fs.accessSync(tlsCA, fs.constants.F_OK | fs.constants.R_OK);
        fs.accessSync(tlsDHParam, fs.constants.F_OK | fs.constants.R_OK);
    } catch (err) {
        // For unit/integration tests, if certs are not found,
        // we generate them into the default cert folder.
        if (process.env.npm_lifecycle_event && process.env.npm_lifecycle_event.startsWith("test")) {
            const execSync = require("child_process").execSync;

            try {
                if (process.platform == "win32") {
                    execSync(createCertsPathWindows);
                } else {
                    execSync(createCertsPathPosix);
                }

                console.log("Created default key, certificate, CA and dhparam.");
            } catch (err) {
                throw Error(
                    "Error while generating the default key, certificate, CA and/or dhparam files: " +
                        err.message
                );
            }
        } else {
            throw Error(
                "The key, certificate, CA and/or dhparam files are missing. They are required because the server won't execute without adequate security."
            );
        }
    }
};
