"use strict";

const path = require("path");

const hello = require(path.resolve("./src/routes/hello.js"));

module.exports = app => {
    app.route("/hello/:name").get(hello.getName);
};
