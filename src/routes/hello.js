"use strict";

module.exports.getName = (req, res) => {
    res.set("content-type", "application/json");
    res.set("charset", "utf-8");
    res.status(200).json({ name: req.params.name });
};
