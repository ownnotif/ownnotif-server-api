# OwnNotif

Take privacy seriously and own your push notifications ✊📢! OwnNotif is a secure, free (libre), open source, self-host push notifications server/clients.

## Server API

The heart of OwnNotif, containing the API.

## Building and Installation

### Node.js

The server is build on top of Node.js 9.x (and striving to be on the next LTS 10.x) and can be downloaded from:

* Direct download: https://nodejs.org/en/download/current/
* Package manager: https://nodejs.org/en/download/package-manager/

We strive to be platform agnostic and to support all the platforms Node.js can be installed on.

### Server

Clone the repository and navigate inside the project's directory. Then do:

```shell
npm install
```

Then you'll be able to test the server with:

```shell
npm test
```

Or start it with:

```shell
npm start
```
